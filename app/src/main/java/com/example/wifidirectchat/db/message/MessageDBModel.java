package com.example.wifidirectchat.db.message;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.ForeignKey;

import com.example.wifidirectchat.db.chat.ChatDBModel;

import java.util.Date;

@Entity(foreignKeys = @ForeignKey(entity = ChatDBModel.class, parentColumns = "id", childColumns = "chatId"), indices = @Index("chatId"))
public class MessageDBModel {
    @PrimaryKey(autoGenerate = true) private int id = 0;
    private int chatId;
    private boolean isMine;
    private String message;
    private Date date;

    public MessageDBModel(int chatId, boolean isMine, String message, Date date) {
        this.chatId = chatId;
        this.isMine = isMine;
        this.message = message;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int getChatId() {
        return chatId;
    }

    public boolean isMine() {
        return isMine;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }
}
