package com.example.wifidirectchat.db.chat;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ChatDao {
    @Query("SELECT c.*, (select count(1) from messagedbmodel m where m.chatId = c.id) messagesCount FROM chatdbmodel c ORDER BY c.lastMessageDate DESC")
    LiveData<List<ChatDBModel.ChatDBModelWrapper>> getAll();

    @Update
    void update(ChatDBModel chat);

    @Insert
    long[] insert(ChatDBModel... chat);

    @Query("DELETE FROM chatdbmodel where id = :id")
    void delete(int id);

    @Query("DELETE FROM chatdbmodel")
    void deleteAll();

    @Query("SELECT c.*, (select count(1) from messagedbmodel m where m.chatId = c.id) messagesCount FROM chatdbmodel c where c.deviceAddress = :deviceAddress")
    ChatDBModel.ChatDBModelWrapper getByDeviceAddress(String deviceAddress);
}
