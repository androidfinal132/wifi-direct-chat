package com.example.wifidirectchat.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.example.wifidirectchat.db.chat.ChatDBModel;
import com.example.wifidirectchat.db.chat.ChatDao;
import com.example.wifidirectchat.db.message.MessageDBModel;
import com.example.wifidirectchat.db.message.MessageDao;
import com.example.wifidirectchat.db.utils.DateTypeConverter;

@Database(entities = {ChatDBModel.class, MessageDBModel.class}, version = 3, exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract ChatDao chatDao();
    public abstract MessageDao messageDao();

    public static AppDatabase getDatabase(Context applicationContext) {
        return Room.databaseBuilder(applicationContext, AppDatabase.class, "database")
                .fallbackToDestructiveMigration()
                .build();
    }
}
