package com.example.wifidirectchat.db.chat;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class ChatDBModel {
    @PrimaryKey(autoGenerate = true) private int id = 0;
    private String phoneName;
    private Date startDate;
    private Date lastMessageDate;
    private String deviceAddress;

    public ChatDBModel(String phoneName, Date startDate, Date lastMessageDate, String deviceAddress) {
        this.phoneName = phoneName;
        this.startDate = startDate;
        this.deviceAddress = deviceAddress;
        this.lastMessageDate = lastMessageDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public static class ChatDBModelWrapper extends ChatDBModel {
        private int messagesCount = 0;

        ChatDBModelWrapper(String phoneName, Date startDate, Date lastMessageDate, String deviceAddress) {
            super(phoneName, startDate, lastMessageDate, deviceAddress);
        }

        public int getMessagesCount() {
            return messagesCount;
        }

        void setMessagesCount(int messagesCount) {
            this.messagesCount = messagesCount;
        }
    }
}
