package com.example.wifidirectchat.db.message;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MessageDao {
    @Query("SELECT * FROM messagedbmodel where chatId = :chatId")
    LiveData<List<MessageDBModel>> getAll(int chatId);

    @Insert
    long[] insert(MessageDBModel... message);

    @Query("DELETE FROM messagedbmodel where chatId = :chatId")
    void delete(int chatId);

    @Query("DELETE FROM messagedbmodel")
    void deleteAll();
}
