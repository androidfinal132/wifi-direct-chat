package com.example.wifidirectchat.utils;

import android.os.AsyncTask;

public abstract class Promise<T> {
    private CallBack<T> callBack;
    private CallBack<Exception> exceptionCallBack;
    private CallBack finalCallBack;
    private T resolvedObject;
    private Exception rejectedException;
    private boolean resolved = false;
    private boolean rejected = false;
    private boolean complete = false;

    protected Promise() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    body();
                } catch(Exception e) {
                    reject(e);
                }
            }
        });
    }

    public interface CallBack<Item> {
        void callBack(Item item);
    }

    public Promise<T> then(CallBack<T> callBack) {
        if (resolved || rejected) return this;
        this.callBack = callBack;
        if (resolvedObject != null)
            resolve();
        return this;
    }

    public Promise<T> onCatch(CallBack<Exception> exceptionCallBack) {
        if (resolved || rejected) return this;
        this.exceptionCallBack = exceptionCallBack;
        if (rejectedException != null)
            reject();
        return this;
    }

    public void onFinally(CallBack finalCallBack) {
        this.finalCallBack = finalCallBack;
        done();
    }

    public abstract void body();

    protected void resolve(T t) {
        if (resolved || rejected) return;
        resolvedObject = t;
        if (callBack != null)
            resolve();
    }

    protected void reject(Exception e) {
        if (resolved || rejected) return;
        rejectedException = e;
        if (exceptionCallBack != null)
            reject();
    }

    private void resolve() {
        try {
            runOnUiThread(this.callBack, resolvedObject);
            resolved = true;
        } catch (Exception e) {
            reject(e);
        } finally {
            done();
        }
    }

    private void reject() {
        try {
            rejected = true;
            runOnUiThread(exceptionCallBack, rejectedException);
        } finally {
            done();
        }
    }

    private void done() {
        if (finalCallBack != null && !complete && (resolved || rejected)) {
            complete = true;
            runOnUiThread(finalCallBack, null);
        }
    }

    private void runOnUiThread(final CallBack callBack, final Object res) {
        callBack.callBack(res);
    }
}
