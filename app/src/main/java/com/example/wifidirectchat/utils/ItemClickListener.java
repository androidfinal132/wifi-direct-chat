package com.example.wifidirectchat.utils;

public interface ItemClickListener<T> {
    void onItemClick(T item, int index);
    void onItemLongClick(T item, int index);
}
