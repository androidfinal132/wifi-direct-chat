package com.example.wifidirectchat.utils;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.wifidirectchat.BR;

import java.util.ArrayList;
import java.util.List;

public class BindableRecyclerAdapter<T extends IdentityInterface> extends RecyclerView.Adapter<BindableRecyclerAdapter.ViewHolder> {
    private List<IdentityInterface> data = new ArrayList<>();
    private int layoutId;
    private ItemClickListener<T> itemClickListener;

    public BindableRecyclerAdapter(int layoutId, ItemClickListener<T> itemClickListener) {
        this.layoutId = layoutId;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public BindableRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        return new BindableRecyclerAdapter.ViewHolder(DataBindingUtil.inflate(inflater, layoutId, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BindableRecyclerAdapter.ViewHolder chatHolder, int i) {
        chatHolder.bind(data.get(i), i);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<IdentityInterface> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding holderBinding;

        ViewHolder(@NonNull ViewDataBinding holderBinding) {
            super(holderBinding.getRoot());
            this.holderBinding = holderBinding;
        }

        void bind(final T item, final int index) {
            holderBinding.setVariable(BR.item, item);
            if (itemClickListener != null)
                holderBinding.setVariable(BR.viewModel, itemClickListener);
            holderBinding.setVariable(BR.index, index);
            holderBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(item, index);
                    }
                }
            });
            holderBinding.getRoot().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemLongClick(item, index);
                        return true;
                    }
                    return false;
                }
            });
            holderBinding.executePendingBindings();
        }
    }
}
