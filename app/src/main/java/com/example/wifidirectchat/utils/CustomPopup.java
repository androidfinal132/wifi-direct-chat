package com.example.wifidirectchat.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.wifidirectchat.R;

public class CustomPopup extends Activity {
    public static Activity ref;
    public static final int DELETE_REQUEST = 1;
    public static final int WIFI_REQUEST = 2;
    public static final int REDISCOVER_REQUEST = 3;
    public static final int CONNECTION_REQUEST = 4;
    TextView messageTextView;
    Button yesButton;
    Button noButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup);
        ref = this;

        String message = getIntent().getStringExtra("message");
        final Parcelable item = getIntent().getParcelableExtra("item");
        messageTextView = findViewById(R.id.popupMessage);
        messageTextView.setText(message);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ConstraintLayout layout = findViewById(R.id.popupId);
        getWindow().setLayout(layout.getLayoutParams().width, layout.getLayoutParams().height);

        yesButton = findViewById(R.id.popupYes);
        noButton = findViewById(R.id.popupNo);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", item);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", item);
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ref = null;
    }
}
