package com.example.wifidirectchat.utils;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import com.example.wifidirectchat.pages.chat.model.Message;
import com.example.wifidirectchat.pages.main.model.Chat;

import java.util.List;

public class BindingUtils {
    private BindingUtils() {}

    @BindingAdapter("data")
    public static void setChatData(RecyclerView recyclerView, List<Chat> data) {
        if (recyclerView.getAdapter() instanceof BindableRecyclerAdapter) {
            ((BindableRecyclerAdapter)recyclerView.getAdapter()).setData(data);
        }
    }

    @BindingAdapter("data")
    public static void setMessageData(RecyclerView recyclerView, List<Message> data) {
        if (recyclerView.getAdapter() instanceof BindableRecyclerAdapter) {
            ((BindableRecyclerAdapter)recyclerView.getAdapter()).setData(data);
        }
    }

    @BindingAdapter("showIf")
    public static void setVisibility(View view, boolean value) {
        view.setVisibility(value ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("margin")
    public static void setMargin(View view, float margin) {
        margin = convertDpToPixel(margin, view.getContext());
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(Math.round(margin), Math.round(margin), Math.round(margin), layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }

    private static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
}
