package com.example.wifidirectchat.pages.chat.model;

import com.example.wifidirectchat.utils.IdentityInterface;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Message implements IdentityInterface {
    private boolean isMine;
    private String message;
    private Date date;
    private int id;

    public Message(boolean isMine, String message, Date date, int id) {
        this.id = id;
        this.isMine = isMine;
        this.message = message;
        this.date = date;
    }

    public boolean isMine() {
        return isMine;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public String getDateFormatted() {
        return new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", new Locale("ka_GE")).format(this.date);
    }

    public void setId(int id) {
        this.id = id;
    }
}
