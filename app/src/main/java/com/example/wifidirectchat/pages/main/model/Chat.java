package com.example.wifidirectchat.pages.main.model;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.wifidirectchat.utils.IdentityInterface;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Chat extends BaseObservable implements Parcelable, Serializable, IdentityInterface {
    private int id;
    private String phoneName;
    private Date startDate;
    private Date lastMessageDate;
    private int messagesCount;
    private String phoneAddress = "";

    public Chat(int id, String phoneName, Date startDate, int messagesCount) {
        this.id = id;
        this.phoneName = phoneName;
        this.startDate = startDate;
        this.messagesCount = messagesCount;
    }

    public Chat(int id, String phoneName, Date startDate, int messagesCount, String phoneAddress) {
        this(id, phoneName, startDate, messagesCount);
        this.phoneAddress = phoneAddress;
    }

    public Chat(int id, String phoneName, Date startDate, int messagesCount, String phoneAddress, Date lastMessageDate) {
        this(id, phoneName, startDate, messagesCount, phoneAddress);
        this.lastMessageDate = lastMessageDate;
    }

    public Chat(Parcel parcel) {
        Chat chat = (Chat) parcel.readSerializable();
        if (chat != null) {
            this.setId(chat.getId());
            this.setPhoneName(chat.getPhoneName());
            this.setStartDate(chat.getStartDate());
            this.setMessagesCount(chat.getMessagesCount());
        }
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    public String getPhoneName() {
        return phoneName;
    }

    private void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public Date getStartDate() {
        return startDate;
    }

    private void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getMessagesCount() {
        return messagesCount;
    }

    private void setMessagesCount(int messagesCount) {
        this.messagesCount = messagesCount;
    }

    public int getId() {
        return id;
    }

    public String getStartDateFormatted() {
        return new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", new Locale("ka_GE")).format(this.startDate);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneAddress() {
        return phoneAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this);
    }

    public String getLastMessageDateFormatted() {
        return new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", new Locale("ka_GE")).format(this.lastMessageDate);
    }
}
