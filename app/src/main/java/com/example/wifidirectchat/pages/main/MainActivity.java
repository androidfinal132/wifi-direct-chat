package com.example.wifidirectchat.pages.main;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.wifidirectchat.R;
import com.example.wifidirectchat.databinding.ActivityMainBinding;
import com.example.wifidirectchat.pages.chat.ChatActivity;
import com.example.wifidirectchat.pages.main.model.Chat;
import com.example.wifidirectchat.utils.BindableRecyclerAdapter;

import static com.example.wifidirectchat.utils.CustomPopup.DELETE_REQUEST;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ActivityMainBinding binding;
    BindableRecyclerAdapter<Chat> adapter;
    MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        viewModel = new MainViewModel(this);

        binding.setViewModel(viewModel);

        adapter = new BindableRecyclerAdapter<>(R.layout.holder_main, viewModel);
        binding.content.recyclerView.setAdapter(adapter);
        binding.content.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        binding.content.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.clearChats();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.fetchDatabase();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DELETE_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.deleteChat((Chat)data.getParcelableExtra("result"));
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_chat) {
            Intent intent = new Intent(this, ChatActivity.class);
            this.startActivity(intent);
            return false;
        } else if (id == R.id.nav_history) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
