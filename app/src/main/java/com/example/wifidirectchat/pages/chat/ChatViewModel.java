package com.example.wifidirectchat.pages.chat;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.example.wifidirectchat.R;
import com.example.wifidirectchat.db.AppDatabase;
import com.example.wifidirectchat.db.chat.ChatDBModel;
import com.example.wifidirectchat.db.message.MessageDBModel;
import com.example.wifidirectchat.pages.chat.model.Message;
import com.example.wifidirectchat.pages.main.model.Chat;
import com.example.wifidirectchat.utils.CustomPopup;
import com.example.wifidirectchat.utils.ItemClickListener;

import java.io.IOException;
import java.io.Serializable;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.wifidirectchat.utils.CustomPopup.CONNECTION_REQUEST;
import static com.example.wifidirectchat.utils.CustomPopup.DELETE_REQUEST;
import static com.example.wifidirectchat.utils.CustomPopup.WIFI_REQUEST;
import static java.lang.Thread.sleep;

public class ChatViewModel extends BaseObservable implements ItemClickListener<Message>, Observer<List<MessageDBModel>>, Serializable {
    public static final int CONNECTION_STATE_DISCONNECTED = 0;
    public static final int CONNECTION_STATE_HAS_PEER = 1;
    public static final int CONNECTION_STATE_HAS_CONNECTION = 2;
    public static final int CONNECTION_STATE_CREATING_SERVER = 3;
    public static final int CONNECTION_STATE_CONNECTING = 4;
    public static final int CONNECTION_STATE_CONNECTED = 5;
    public static final int CONNECTION_STATE_CHATTING = 6;
    private static final int PERMISSIONS_REQUEST = 1;
    private static final int SOCKET_ACTION_CLOSED = 0;
    private static final int SOCKET_ACTION_MESSAGE = 1;

    private final IntentFilter intentFilter = new IntentFilter();
    private final ObservableArrayList<Message> messages = new ObservableArrayList<>();
    private ObservableField<Chat> chat = new ObservableField<>(null);
    private ObservableBoolean isHistory = new ObservableBoolean(true);
    private ObservableInt connectionState = new ObservableInt(CONNECTION_STATE_DISCONNECTED);
    private ObservableBoolean isWifiP2pEnabled = new ObservableBoolean(false);
    private ObservableField<String> msg = new ObservableField<>("");
    private ObservableInt openId = new ObservableInt(0);
    private ChatActivity context;
    private AppDatabase db;
    private WifiManager wifiManager;
    private WifiP2pManager.Channel channel;
    private WifiP2pManager manager;
    private WifiP2pBroadcastReceiver receiver;
    private Socket socket;
    private Set<String> userDeclinedDeviceAddresses = new HashSet<>();
    private ServerSocket listenSocket;
    private String hostAddress = "";
    private LiveData<List<MessageDBModel>> liveData;
    private boolean accessFineLocationPermission;
    private boolean accessWifiStatePermission;
    private boolean changeWifiStatePermission;
    private boolean internetPermission;

    public ChatViewModel(final Chat chat, ChatActivity context) {
        this.context = context;
        this.db = AppDatabase.getDatabase(context.getApplicationContext());
        accessFineLocationPermission = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        accessWifiStatePermission = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED;
        changeWifiStatePermission = ActivityCompat.checkSelfPermission(context, Manifest.permission.CHANGE_WIFI_STATE) == PackageManager.PERMISSION_GRANTED;
        internetPermission = ActivityCompat.checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED;
        if (chat == null) {
            isHistory.set(false);
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
            wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (allPermissionsGranted() && isWifiEnabled()) {
                startP2p();
            } else if (allPermissionsGranted()) {
                promptWifi();
            } else {
                requestPermissions();
            }
        }
        this.chat.set(chat == null ? new Chat(0, "", new Date(), 0) : chat);
    }

    @Override
    public void onItemClick(Message item, int index) {
        if (this.openId.get() == item.getId()) {
            this.openId.set(0);
        } else {
            this.openId.set(item.getId());
            context.scrollTo(index);
        }
    }

    @Override
    public void onItemLongClick(Message item, int index) {

    }

    @Override
    public void onChanged(@Nullable List<MessageDBModel> messageDBModels) {
        liveData.removeObserver(this);
        if (messageDBModels == null) return;
        List<Message> list = new ArrayList<>();
        for (MessageDBModel messageDBModel: messageDBModels) {
            list.add(new Message(messageDBModel.isMine(), messageDBModel.getMessage(), messageDBModel.getDate(), messageDBModel.getId()));
        }
        messages.clear();
        messages.addAll(list);
        context.scrollToBottom();
    }

    public boolean isMessageDifferent(int i) {
        if (i > 0 && this.messages.size() > i && this.messages.get(i - 1).isMine() == this.messages.get(i).isMine()) {
            return this.messages.get(i).getDate().getTime() - this.messages.get(i - 1).getDate().getTime() < 180000;
        }
        return false;
    }

    public ObservableArrayList<Message> getMessages() {
        return messages;
    }

    public ObservableInt getConnectionState() {
        return connectionState;
    }

    public ObservableBoolean isHistory() {
        return isHistory;
    }

    public ObservableField<Chat> getChat() {
        return chat;
    }

    public ObservableField<String> getMsg() {
        return msg;
    }

    public ObservableInt getOpenId() {
        return openId;
    }

    void setIsWifiP2pEnabled(boolean b) {
        if (b && !isWifiP2pEnabled.get()) {
            startP2p();
        }
        isWifiP2pEnabled.set(b);
    }

    void wifiP2pPeersChangedAction(WifiP2pDeviceList deviceList) {
        List<WifiP2pDevice> devices = new ArrayList<>();
        for (WifiP2pDevice d : deviceList.getDeviceList()) {
            if (!userDeclinedDeviceAddresses.contains(d.deviceAddress))
                devices.add(d);
        }
        if (!devices.isEmpty() && connectionState.get() == CONNECTION_STATE_DISCONNECTED) {
            closePopup();
            connectionState.set(CONNECTION_STATE_HAS_PEER);
            promptConnection(devices.get(0));
        }
    }

    void connectionChangedAction(final NetworkInfo networkInfo, final Intent intent) {
        if (networkInfo.isConnected()) {
            if (connectionState.get() >= CONNECTION_STATE_HAS_CONNECTION) return;
            closePopup();
            connectionState.set(CONNECTION_STATE_HAS_CONNECTION);
            manager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
                @Override
                public void onConnectionInfoAvailable(final WifiP2pInfo info) {
                    WifiP2pGroup group = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP);
                    startChat(info, group);
                }
            });
        } else {
            disconnect();
        }
    }

    void discoveryChanged() {
        startDiscovery();
    }

    void turnWifi() {
        this.wifiManager.setWifiEnabled(true);
        startP2p();
    }

    void onDelete() {
        Intent intent = new Intent(context, CustomPopup.class);
        Chat chat = this.chat.get();
        if (chat != null)
            intent.putExtra("message", context.getResources().getString(R.string.popup_message, chat.getPhoneName()));
        intent.putExtra("item", (Parcelable) chat);
        context.startActivityForResult(intent, DELETE_REQUEST);
    }

    void deleteChat(final Chat chat) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                db.messageDao().delete(chat.getId());
                db.chatDao().delete(chat.getId());
            }
        }).start();
        context.onBackPressed();
    }

    void fetchDatabase() {
        Chat chat = this.chat.get();
        if (chat != null) {
            liveData = db.messageDao().getAll(chat.getId());
            liveData.observe(context, this);
        }
    }

    void sendMessage() {
        final String msg = this.msg.get();
        if (msg == null || msg.length() <= 0) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    byte[] text = msg.getBytes();
                    byte[] buffer = new byte[text.length + 1];
                    buffer[0] = (byte) SOCKET_ACTION_MESSAGE;
                    System.arraycopy(text, 0, buffer, 1, text.length);
                    socket.getOutputStream().write(buffer);
                    addMessage(new Message(true, getMsg().get(), new Date(), 0));
                    getMsg().set("");
                } catch (IOException e) {
                    disconnect();
                    disconnectWifiDirect();
                    startDiscovery();
                }
            }
        }).start();
    }

    void setDeviceAccepted(final WifiP2pDevice device, boolean accepted) {
        if (accepted) {
            startConnectionWith(device);
        } else {
            if (device != null)
                userDeclinedDeviceAddresses.add(device.deviceAddress);
            if (connectionState.get() == CONNECTION_STATE_HAS_PEER)
                connectionState.set(CONNECTION_STATE_DISCONNECTED);
            startDiscovery();
        }
    }

    IntentFilter getIntentFilter() {
        return intentFilter;
    }

    WifiP2pBroadcastReceiver getReceiver() {
        return receiver;
    }

    void startDiscovery() {
        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int i) {
            }
        });
    }

    void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                accessFineLocationPermission = true;
            }
            if (permission.equals(Manifest.permission.ACCESS_WIFI_STATE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                accessWifiStatePermission = true;
            }
            if (permission.equals(Manifest.permission.CHANGE_WIFI_STATE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                changeWifiStatePermission = true;
            }
            if (permission.equals(Manifest.permission.INTERNET) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                internetPermission = true;
            }
        }
        if (allPermissionsGranted()) {
            if (isWifiEnabled()) {
                startP2p();
            } else if (allPermissionsGranted()) {
                promptWifi();
            }
        }
    }

    void onBackPressed() {
        connectionState.set(CONNECTION_STATE_DISCONNECTED);
        disconnectWifiDirect();
        stopDiscovery();
        closeSocket();
    }

    private boolean isWifiEnabled() {
        return wifiManager.isWifiEnabled();
    }

    private boolean allPermissionsGranted(){
        return accessFineLocationPermission && accessWifiStatePermission && changeWifiStatePermission && internetPermission;
    }

    private void startP2p() {
        isWifiP2pEnabled.set(true);
        manager = (WifiP2pManager) context.getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(context, context.getMainLooper(), new WifiP2pManager.ChannelListener() {
            @Override
            public void onChannelDisconnected() {
            }
        });
        manager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
            @Override
            public void onGroupInfoAvailable(final WifiP2pGroup group) {
                if (group != null) {
                    manager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
                        @Override
                        public void onConnectionInfoAvailable(WifiP2pInfo info) {
                            startChat(info, group);
                            receiver = new WifiP2pBroadcastReceiver(ChatViewModel.this);
                            startDiscovery();
                            context.startReceiver();
                        }
                    });
                } else {
                    receiver = new WifiP2pBroadcastReceiver(ChatViewModel.this);
                    startDiscovery();
                    context.startReceiver();
                }
            }
        });
    }

    private void promptWifi() {
        Intent intent = new Intent(context, CustomPopup.class);
        intent.putExtra("message", "გსურთ ჩართოთ wi-fi?");
        context.startActivityForResult(intent, WIFI_REQUEST);
    }

    private void requestPermissions() {
        if (!accessFineLocationPermission)
            ActivityCompat.requestPermissions(context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.CHANGE_WIFI_STATE,
                            Manifest.permission.INTERNET}, PERMISSIONS_REQUEST);
    }

    private void closePopup() {
        if (CustomPopup.ref != null && !CustomPopup.ref.isFinishing())
            CustomPopup.ref.finish();
    }

    private void startChat(final WifiP2pInfo info, final WifiP2pGroup group) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                establishSocketConnection(info);
                if (connectionState.get() == CONNECTION_STATE_CONNECTED) {
                    fetchDeviceInfo(group);
                    getMessagesLoop();
                }
            }
        }).start();
    }

    private void disconnect() {
        closeSocket();
        connectionState.set(CONNECTION_STATE_DISCONNECTED);
    }

    private void promptConnection(WifiP2pDevice device) {
        Intent intent = new Intent(context, CustomPopup.class);
        intent.putExtra("message", String.format(context.getResources().getString(R.string.popup_connect), device.deviceName));
        intent.putExtra("item", device);
        context.startActivityForResult(intent, CONNECTION_REQUEST);
    }

    private void stopDiscovery() {
        if (manager != null) {
            manager.stopPeerDiscovery(channel, null);
        }
    }

    private void disconnectWifiDirect() {
        if (manager != null) {
            manager.removeGroup(channel, null);
            manager.cancelConnect(channel, null);
        }
    }

    private void addMessage(final Message message) {
        Chat chat = this.chat.get();
        if (chat != null) {
            message.setId((int)db.messageDao().insert(new MessageDBModel(chat.getId(), message.isMine(), message.getMessage(), message.getDate()))[0]);
            ChatDBModel chatDBModel = new ChatDBModel(chat.getPhoneName(), chat.getStartDate(), message.getDate(), chat.getPhoneAddress());
            chatDBModel.setId(chat.getId());
            db.chatDao().update(chatDBModel);
            messages.add(message);
            context.scrollToBottom();
        }
    }

    private void getMessagesLoop() {
        byte[] buffer = new byte[1024];
        int nReceived;
        while (connectionState.get() == CONNECTION_STATE_CHATTING) {
            try {
                if (socket == null) throw new Exception();
                if (!socket.isConnected()) {
                    if (!socket.isClosed()) {
                        socket.close();
                    }
                    socket = null;
                    throw new Exception();
                }
                nReceived = socket.getInputStream().read(buffer);
                if (nReceived > -1) {
                    int actionCode = (int)buffer[0];
                    if (actionCode == SOCKET_ACTION_MESSAGE)
                        addMessage(new Message(false, new String(buffer, 1, nReceived - 1), new Date(), 0));
                    else if (actionCode == SOCKET_ACTION_CLOSED) {
                        disconnect();
                        disconnectWifiDirect();
                        startDiscovery();
                    } else {
                        addMessage(new Message(false, new String(buffer, 0, nReceived), new Date(), 0));
                    }
                }
            } catch (Exception e) {
                disconnect();
            }
        }
    }

    private void closeSocket() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (socket != null && !socket.isClosed()) {
                        if (socket.isConnected()) {
                            socket.getOutputStream().write(new byte[]{(byte) SOCKET_ACTION_CLOSED});
                        }
                        socket.close();
                    }
                    if (listenSocket != null && !listenSocket.isClosed())
                        listenSocket.close();
                    socket = null;
                } catch (Exception e) {
                }
            }
        }).start();
    }

    private void establishSocketConnection(WifiP2pInfo info) {
        final InetAddress inetAddress = info.groupOwnerAddress;
        if (info.groupFormed && info.isGroupOwner) {
            createServer();
        } else if (info.groupFormed) {
            hostAddress = inetAddress.getHostAddress();
            connectToServer();
        }
    }

    private void connectToServer() {
        connectionState.set(CONNECTION_STATE_CONNECTING);
        long startTime = new Date().getTime();
        while (true) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(hostAddress, 8888), 10000);
                connectionState.set(CONNECTION_STATE_CONNECTED);
                break;
            } catch (Exception e) {
                try {
                    sleep(1000);
                } catch (Exception ignored) {}
                if (new Date().getTime() - startTime >= 30000) {
                    connectionState.set(CONNECTION_STATE_DISCONNECTED);
                    disconnectWifiDirect();
                    startDiscovery();
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context.getApplicationContext(), context.getResources().getText(R.string.failed_connecting), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                }
            }
        }
    }

    private void createServer() {
        try {
            if (listenSocket == null || listenSocket.isClosed()) {
                listenSocket = new ServerSocket();
                listenSocket.setReuseAddress(true);
                listenSocket.bind(new InetSocketAddress(8888));
            }
            connectionState.set(CONNECTION_STATE_CREATING_SERVER);
            socket = listenSocket.accept();
            connectionState.set(CONNECTION_STATE_CONNECTED);
        } catch (Exception e) {
            if (e instanceof BindException && (e).getMessage().contains("EADDRINUSE")) {
                try {
                    sleep(1000);
                } catch (Exception ignored) {}
                createServer();
            } else {
                e.printStackTrace();
            }
        }
    }

    private void fetchDeviceInfo(WifiP2pGroup group) {
        WifiP2pDevice device;
        Collection<WifiP2pDevice> mClients = group.getClientList();
        if (mClients.size() > 0) {
            device = new ArrayList<>(mClients).get(0);
        } else {
            device = group.getOwner();
        }
        if (device != null) {
            ChatDBModel.ChatDBModelWrapper chatDBModel = db.chatDao().getByDeviceAddress(device.deviceAddress);
            Chat chat;
            if (chatDBModel != null) {
                chat = new Chat(chatDBModel.getId(), chatDBModel.getPhoneName(), chatDBModel.getStartDate(), chatDBModel.getMessagesCount(), chatDBModel.getDeviceAddress());
            } else {
                chat = new Chat(0, device.deviceName, new Date(), 0, device.deviceAddress);
                chat.setId((int) db.chatDao().insert(new ChatDBModel(chat.getPhoneName(), chat.getStartDate(), new Date(), chat.getPhoneAddress()))[0]);
            }
            ChatViewModel.this.chat.set(chat);
            fetchDatabase();
            connectionState.set(CONNECTION_STATE_CHATTING);
        }
    }

    private void startConnectionWith(final WifiP2pDevice device) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;

        manager.connect(channel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            sleep(30000);
                        } catch (Exception ignored) {}
                        if (connectionState.get() == CONNECTION_STATE_HAS_PEER) {
                            connectionState.set(CONNECTION_STATE_DISCONNECTED);
                            startDiscovery();
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context.getApplicationContext(), context.getResources().getText(R.string.failed_connecting), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }.start();
            }

            @Override
            public void onFailure(int i) {
                connectionState.set(CONNECTION_STATE_DISCONNECTED);
            }
        });
    }
}
