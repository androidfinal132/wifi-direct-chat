package com.example.wifidirectchat.pages.chat;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;

import com.example.wifidirectchat.R;
import com.example.wifidirectchat.databinding.ActivityChatBinding;
import com.example.wifidirectchat.pages.chat.model.Message;
import com.example.wifidirectchat.pages.main.model.Chat;
import com.example.wifidirectchat.utils.BindableRecyclerAdapter;

import static com.example.wifidirectchat.utils.CustomPopup.CONNECTION_REQUEST;
import static com.example.wifidirectchat.utils.CustomPopup.DELETE_REQUEST;
import static com.example.wifidirectchat.utils.CustomPopup.REDISCOVER_REQUEST;
import static com.example.wifidirectchat.utils.CustomPopup.WIFI_REQUEST;

public class ChatActivity extends AppCompatActivity {
    private ChatViewModel viewModel;
    private ActivityChatBinding binding;
    private boolean isOpened = false;

    private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            final View activityRootView = findViewById(R.id.chatContent);
            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
            if (heightDiff > 300) {
                if (!isOpened) {
                    scrollToBottom();
                    isOpened = true;
                }
            } else if (isOpened) {
                scrollToBottom();
                isOpened = false;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Chat chat = (Chat) getIntent().getSerializableExtra("chat");
        viewModel = new ChatViewModel(chat, this);
        BindableRecyclerAdapter<Message> recyclerAdapter = new BindableRecyclerAdapter<>(R.layout.holder_chat, viewModel);
        binding.setViewModel(viewModel);
        binding.chatContent.recyclerView2.setAdapter(recyclerAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.chatContent.recyclerView2.setLayoutManager(linearLayoutManager);

        binding.chatContent.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.sendMessage();
            }
        });
        binding.cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Parcelable result = null;
        try {
            if (data.getParcelableExtra("result") != null)
                result = data.getParcelableExtra("result");
        } catch(Exception ignored) {}
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DELETE_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.deleteChat((Chat)result);
            }
        } else if (requestCode == WIFI_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.turnWifi();
            } else {
                onBackPressed();
            }
        } else if (requestCode == REDISCOVER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.startDiscovery();
            } else {
                onBackPressed();
            }
        } else if (requestCode == CONNECTION_REQUEST) {
            viewModel.setDeviceAccepted((WifiP2pDevice)result, resultCode == Activity.RESULT_OK);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.fetchDatabase();
        startReceiver();
        setOnGlobalLayoutListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopReceiver();
        viewModel.onBackPressed();
        removeOnGlobalLayoutListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (viewModel.isHistory().get())
            getMenuInflater().inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete) {
            viewModel.onDelete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (viewModel != null)
            viewModel.onRequestPermissionsResult(permissions, grantResults);
    }

    public void scrollToBottom() {
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(50);
                } catch (Exception ignored) {}
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (binding.chatContent.recyclerView2 != null && binding.chatContent.recyclerView2.isShown())
                            binding.chatContent.recyclerView2.getLayoutManager().smoothScrollToPosition(binding.chatContent.recyclerView2, null, Math.max(viewModel.getMessages().size() - 1, 0));
                    }
                });
            }
        }.start();
    }

    public void scrollTo(final int index) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(50);
                } catch (Exception ignored) {}
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (binding.chatContent.recyclerView2 != null && binding.chatContent.recyclerView2.isShown())
                            binding.chatContent.recyclerView2.getLayoutManager().smoothScrollToPosition(binding.chatContent.recyclerView2, null, index);
                    }
                });
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        viewModel.onBackPressed();
        super.onBackPressed();
    }

    void startReceiver() {
        if (viewModel.getReceiver() != null) {
            registerReceiver(viewModel.getReceiver(), viewModel.getIntentFilter());
        }
    }

    private void setOnGlobalLayoutListener() {
        final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    private void removeOnGlobalLayoutListener() {
        final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    private void stopReceiver() {
        if (viewModel.getReceiver() != null) {
            unregisterReceiver(viewModel.getReceiver());
        }
    }

}
