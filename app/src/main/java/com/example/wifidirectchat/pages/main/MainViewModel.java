package com.example.wifidirectchat.pages.main;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.example.wifidirectchat.R;
import com.example.wifidirectchat.db.AppDatabase;
import com.example.wifidirectchat.db.chat.ChatDBModel;
import com.example.wifidirectchat.pages.chat.ChatActivity;
import com.example.wifidirectchat.pages.main.model.Chat;
import com.example.wifidirectchat.utils.CustomPopup;
import com.example.wifidirectchat.utils.ItemClickListener;
import com.example.wifidirectchat.utils.Promise;

import java.util.ArrayList;
import java.util.List;

import static com.example.wifidirectchat.utils.CustomPopup.DELETE_REQUEST;

public class MainViewModel extends BaseObservable implements ItemClickListener<Chat>, Observer<List<ChatDBModel.ChatDBModelWrapper>> {

    private ObservableList<Chat> chats = new ObservableArrayList<>();
    private MainActivity context;
    private AppDatabase db;

    MainViewModel(MainActivity context) {
        this.context = context;
        db = AppDatabase.getDatabase(context.getApplicationContext());
    }

    public ObservableList<Chat> getChats() {
        return chats;
    }

    @Override
    public void onItemClick(Chat chat, int index) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("chat", (Parcelable) chat);
        context.startActivity(intent);
    }

    @Override
    public void onItemLongClick(Chat chat, int index) {
        Intent intent = new Intent(context, CustomPopup.class);
        intent.putExtra("message", context.getResources().getString(R.string.popup_message, chat.getPhoneName()));
        intent.putExtra("item", (Parcelable) chat);
        context.startActivityForResult(intent, DELETE_REQUEST);
    }

    @Override
    public void onChanged(@Nullable List<ChatDBModel.ChatDBModelWrapper> chatDBModels) {
        if (chatDBModels == null) return;
        List<Chat> list = new ArrayList<>();
        for (ChatDBModel.ChatDBModelWrapper chatDbModel: chatDBModels) {
            list.add(new Chat(
                    chatDbModel.getId(),
                    chatDbModel.getPhoneName(),
                    chatDbModel.getStartDate(),
                    chatDbModel.getMessagesCount(),
                    chatDbModel.getDeviceAddress(),
                    chatDbModel.getLastMessageDate()
            ));
        }
        chats.clear();
        chats.addAll(list);
    }

    void clearChats() {
        new Promise() {
            @Override
            public void body() {
                db.messageDao().deleteAll();
                db.chatDao().deleteAll();
            }
        };
        chats.clear();
    }

    void deleteChat(final Chat chat) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                db.messageDao().delete(chat.getId());
                db.chatDao().delete(chat.getId());
            }
        }).start();
    }

    void fetchDatabase() {
        this.db.chatDao().getAll().observe(context, this);
    }
}
